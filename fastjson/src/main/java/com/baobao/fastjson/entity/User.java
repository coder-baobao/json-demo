package com.baobao.fastjson.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author baobao
 * @create 2021-03-12 16:44
 * @description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @JSONField(name = "userName")
    private String name;
    @JSONField(name = "userAge")
    private Integer age;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date birth;
}
