package com.baobao.fastjson.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baobao.fastjson.entity.User;
import javafx.collections.ObservableMap;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author baobao
 * @create 2021-03-12 16:46
 * @description
 */
public class FastjsonTest {
    @Test
    public void testSerialize(){
        User user = new User("baobao", 18,new Date());
        String json = JSON.toJSONString(user);
        System.out.println(json);
    }

    @Test
    public void testDeserialize(){
        String json = "{\"birth\":\"2021-03-12\",\"userAge\":18,\"userName\":\"baobao\"}";
        User user = JSON.parseObject(json, User.class);
        System.out.println(user.getName() + ":" + user.getAge() + ":" + user.getBirth());
    }

    @Test
    public void testArray(){
        User user1 = new User("baobao1", 18,new Date());
        User user2 = new User("baobao2", 18,new Date());
        User user3 = new User("baobao3", 18,new Date());
        // 序列化
        User[] userArray = {user1, user2, user3};
        String json = JSON.toJSONString(userArray);
        System.out.println(json);
        // 反序列化
        User[] users = JSON.parseObject(json, User[].class);
        for (User user : users) {
            System.out.println(user);
        }
    }

    @Test
    public void testList(){
        User user1 = new User("baobao1", 18,new Date());
        User user2 = new User("baobao2", 18,new Date());
        User user3 = new User("baobao3", 18,new Date());
        // 序列化
        List<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        String json = JSON.toJSONString(userList);
        System.out.println(json);
        // 反序列化
        List<User> users = JSON.parseArray(json, User.class);
        for (User user : users) {
            System.out.println(user);
        }
    }

    @Test
    public void testMap(){
        // 反序列化
        String json = "{\"birth\":\"2021-03-12\",\"userAge\":18,\"userName\":\"baobao\"}";
        // 方式一:转换为Map
        Map map = JSON.parseObject(json, Map.class);
        // 方式二：转换为JSONObject，它继承自Map
        JSONObject jsonObject = JSON.parseObject(json);
        System.out.println(map.get("userName") + ":" + map.get("userAge") + ":" + map.get("birth"));
        System.out.println(jsonObject.get("userName") + ":" + jsonObject.get("userAge") + ":" + jsonObject.get("birth"));

        // 序列化
        String json1 = JSON.toJSONString(map);
        String json2 = JSON.toJSONString(jsonObject);
        System.out.println(json1);
        System.out.println(json2);
    }
}
