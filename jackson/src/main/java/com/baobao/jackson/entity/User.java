package com.baobao.jackson.entity;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @author baobao
 * @create 2021-03-12 17:38
 * @description
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {
    @JsonProperty("userName")
    private String name;
    private Integer age;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date birth;
}
