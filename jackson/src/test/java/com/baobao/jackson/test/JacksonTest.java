package com.baobao.jackson.test;

import com.baobao.jackson.entity.User;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;

import java.text.DateFormat;
import java.util.*;

/**
 * @author baobao
 * @create 2021-03-12 17:42
 * @description
 */
public class JacksonTest {
    @Test
    public void testSerialize() throws Exception {
        // 序列化
        User user = new User("baobao", 18,new Date());
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(user);
        System.out.println(json);

        // 反序列化
        User parsedUser = objectMapper.readValue(json, User.class);
        System.out.println(parsedUser);
    }

    @Test
    public void testArray() throws Exception {
        User user1 = new User("baobao1", 18,new Date());
        User user2 = new User("baobao2", 18,new Date());
        User user3 = new User("baobao3", 18,new Date());
        User[] userArray = {user1, user2, user3};
        // 序列化
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(userArray);
        System.out.println(json);
        // 反序列化
        User[] users = objectMapper.readValue(json, User[].class);
        for (User user : users) {
            System.out.println(user);
        }
    }

    @Test
    public void testList() throws Exception {
        User user1 = new User("baobao1", 18,new Date());
        User user2 = new User("baobao2", 18,new Date());
        User user3 = new User("baobao3", 18,new Date());
        List<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        ObjectMapper objectMapper = new ObjectMapper();
        // 序列化
        String json = objectMapper.writeValueAsString(userList);
        System.out.println(json);
        // 反序列化
        // 方式一：反序列化后不带泛型
        List list = objectMapper.readValue(json, List.class);
        for (Object o : list) {
            System.out.println(o);
        }
        // 方式二：序列化后携带泛型，方便使用
        List<User> users = objectMapper.readValue(json, new TypeReference<List<User>>() {});
        for (User user : users) {
            System.out.println(user);
        }
    }

    @Test
    public void testMap() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        // 序列化Map
        Map<String, Object> map = new HashMap<>();
        map.put("birth", new Date());
        map.put("name", "baobao");
        map.put("age", 18);
        map.put("address", "zjut");
        String json = objectMapper.writeValueAsString(map);
        System.out.println(json);
        // 反序列化为Map
        Map<String, Object> parsedMap = objectMapper.readValue(json, new TypeReference<Map<String, Object>>(){});
        System.out.println(parsedMap);
    }

    @Test
    public void testNull() throws Exception {
        User user = new User("baobao", 18, new Date());
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(user);
        System.out.println(json);
        String json1 = "{\"birth\":\"2021-03-12\",\"age\":18,\"userName\":\"baobao\"}";
        User value = objectMapper.readValue(json1, User.class);
        System.out.println(value);
    }

    @Test
    public void testNotRecognizedField() throws Exception {
        String json = "{\"birthday\":\"2021-03-12\",\"age\":18,\"userName\":\"baobao\"}";
        ObjectMapper objectMapper = new ObjectMapper();
        // 设置反序列化碰到无法识别的字段时不抛出异常
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        User user = objectMapper.readValue(json, User.class);
        System.out.println(user);
    }

    @Test
    public void testVisibility() throws Exception {
        // 序列化
        User user = new User("baobao", 18,new Date());
        ObjectMapper objectMapper = new ObjectMapper();
        // 设置private的属性也能可视化
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String json = objectMapper.writeValueAsString(user);
        System.out.println(json);
        // 反序列化
        User parsedUser = objectMapper.readValue(json, User.class);
        System.out.println(parsedUser);
    }

    @Test
    public void testGlobalConfigure() {
        ObjectMapper objectMapper = new ObjectMapper();
        // 对于空的对象转json的时候不抛出错误
        objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        // 允许属性名称没有引号
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        // 允许属性名称用单引号
        objectMapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        // 反序列化时忽略在json字符串中存在但在java对象实际没有的属性，不抛出异常
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        // 序列化时结果不包含值为null的属性
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
}
